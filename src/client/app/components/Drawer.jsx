import React from 'react';
import {render} from 'react-dom';
import cx from 'classnames';

export default class Footer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    static get defaultProps() {
        return {
        }
    }

    render () {
        const { wrapperCls, title, children } = this.props;

        return (
            <div className='sp-drawer'>
                <h1 className='sp-drawer-title'>{title}</h1>

                <div className={cx('sp-drawer-content', wrapperCls)}>
                    {children}
                </div>
            </div>
        );
    }
}