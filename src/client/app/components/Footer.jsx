import React from 'react';
import {render} from 'react-dom';

export default class Footer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    static get defaultProps() {
        return {
        }
    }

    render () {
        return (
            <div className='sp-footer'>
                <span>SuperPerspective &copy; Peter Aquila - Arend Castelein - Nicholas Shooter - Larry Smith - Daniel Xiao </span>
            </div>
        );
    }
}