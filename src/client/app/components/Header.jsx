import React from 'react';
import {render} from 'react-dom';

import Loader from '../components/Loader.jsx';

class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loaded: false
        };
        this.handleOnLoaded = this.handleOnLoaded.bind(this);

    }

	static get defaultProps() {
		return {
			children: [],
            currPage: 'home',
            promiseHeaderClick: () => Promise.resolve(/*src, action*/),
		}
	}

    render () {
    	let { children, currPage } = this.props;
        let { loaded } = this.state;

        return (
        	<div className='sp-header'>
                {/*TODO: Build carousel component*/}
                <div className='sp-carousel'>
                    <div className='sp-carousel-header'>
                        <h1 className='title'>SuperPerspective</h1>
                        <img className='floating logo' src='./media/hoodie-silhoutte.png'></img>
                    </div>
                    <div className='sp-carousel-body'>
                        <video className='sp-full-video' loop='loop' autoPlay='autoplay' onCanPlay={this.handleOnLoaded}>
                            <source type="video/mp4" src='./media/videos/index_video.mp4' />
                        </video>

                        <Loader active={!loaded} />
                    </div>
                    <div className='sp-carousel-footer'>
                        <img className='sp-carousel-image' src='./media/jumping-silhouette.png' />
                    </div>
                </div>


            </div>
        );
    }

    handleHeaderClick(action) {
        return Promise.all([this.props.promiseHeaderClick(action)]);
    }

    handleOnLoaded() {
        this.setState({ loaded: true });
        // return Promise.all([this.props.promiseDidLoad()]);
    }
}

Header.PAGE = {
    HOME: 'home',
    ABOUT: 'about',
    MEDIA: 'media',
}

export default Header;
