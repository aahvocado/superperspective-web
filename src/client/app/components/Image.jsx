import React from 'react';
import {render} from 'react-dom';

import Loader from '../components/Loader.jsx';

export default class Image extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loaded: false,
        };

        this.handleOnLoaded = this.handleOnLoaded.bind(this);
    }

    static get defaultProps() {
        return {
            wrapperCls: '',
            src: '',
            promiseDidLoad: () => Promise.resolve()
        }
    }

    render () {
        const { wrapperCls, src } = this.props;
        let { loaded } = this.state;

        return (
            <span className={wrapperCls}>
                <img src={src} onLoad={this.handleOnLoaded}/>
                <Loader active={ !loaded }/>
            </span>
        );

        
    }

    handleOnLoaded() {
        this.setState({ loaded: true });
        return Promise.all([this.props.promiseDidLoad()]);
    }
}