import React from 'react';
import {render} from 'react-dom';

import MediaBox from '../components/MediaBox.jsx';
import Image from '../components/Image.jsx';

export default class ImageGallery extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        };
    }

    static get defaultProps() {
        return {
            layout: ImageGallery.LAYOUT.LINEAR
        }
    }

    render () {
        let { layout } = this.props;

        switch(layout) {
            case ImageGallery.LAYOUT.ONEBIG:
                return (
                    <div className='sp-image-gallery mod-one-big'>
                        <span className='media-box-primary'>
                            <MediaBox caption='SuperPerspective is a fun perspective shifting puzzle platforming game.'>
                                <Image src='./media/sp-img1.png' />
                            </MediaBox>
                        </span>

                        <span className='media-box-secondary'>
                            <MediaBox caption='Exciting worlds with a unique way to view them await.'>
                                <Image src='./media/grass6.PNG' />
                            </MediaBox>
    
                            <MediaBox caption='Clever puzzles for those whose minds can change perspectives.'>
                                <Image src='./media/desert5.PNG' />
                            </MediaBox>
    
                            <MediaBox caption='Find the crystals to revive the worlds.'>
                                <Image src='./media/ice8.PNG' />
                            </MediaBox>
                        </span>
                    </div>
                );
                break;
            case ImageGallery.LAYOUT.LINEAR:
            default:
                return (
                    <div className='sp-image-gallery mod-linear'>
                        <MediaBox caption='SuperPerspective is a fun perspective shifting puzzle platforming game.'>
                            <Image src='./media/sp-img1.png' />
                        </MediaBox>

                        <MediaBox caption='Explore the world of grass and shrooms.'>
                            <Image src='./media/grass6.PNG' />
                        </MediaBox>

                        <MediaBox caption='Sweat in the desert of shifting sands.'>
                            <Image src='./media/desert5.PNG' />
                        </MediaBox>

                        <MediaBox caption='Come chill with the coolest ice puzzles.'>
                            <Image src='./media/ice8.PNG' />
                        </MediaBox>
                    </div>
                );
                break;
        }
        
    }

    handleOnLoaded() {
        this.setState({ loaded: true });
    }
}

ImageGallery.LAYOUT = {
    LINEAR: 'home',
    ONEBIG: 'one-big'
}