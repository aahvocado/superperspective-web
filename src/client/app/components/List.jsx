import React from 'react';
import {render} from 'react-dom';

class List extends React.Component {
	constructor(props) {
		super(props);
		this.state = {

		};
	}
	static get defaultProps() {
		return {
			wrapperCls: 'sp-list',
			listItemComponent: undefined,
			data: [],
		}
	}
	render () {
		let { wrapperCls, data } = this.props;
		let content = data.map(item => {
			return (this.renderListItem(item));
		});
		return (
			<ul className={wrapperCls}>
				{content}
			</ul>
		);
	}

	renderListItem(data) {
		let { listItemComponent } = this.props;
		let { title } = data;

		if(listItemComponent !== undefined){
			return (
				<li className='sp-li'
					key={title + '-list-item'}>
					<span>{title}</span>
				</li>
			)
		}else{
			return (
				<li className='sp-li'
					key={title + '-list-item'}>
					<span>{title}</span>
				</li>
			);
		}
	}
}
export default List;