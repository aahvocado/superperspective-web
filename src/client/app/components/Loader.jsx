import React from 'react';
import {render} from 'react-dom';

import cx from 'classnames';

export default class Loader extends React.Component {
	constructor(props) {
		super(props);
		this.state = {

		};
	}
	static get defaultProps() {
		return {
			active: true
		}
	}
	render () {
		const { active } = this.props;

		return (
			<img className={cx('loading', {'disabled':!active})} src='./media/loading-cube.svg' disabled={!active} />
		);
	}

}