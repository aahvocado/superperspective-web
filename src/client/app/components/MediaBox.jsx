import React from 'react';
import {render} from 'react-dom';

import cx from 'classnames';
import Loader from '../components/Loader.jsx';

export default class MediaBox extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			isFullScreen: false,
			isHover: false
		};

		this.toggleFullScreen = this.toggleFullScreen.bind(this);

		this.handleMouseOver = this.handleMouseOver.bind(this);
		this.handleMouseOut = this.handleMouseOut.bind(this);
	}
	static get defaultProps() {
		return {
			cssMods: 'mod-floating',
			caption: '',
			canFullScreen: true
		}
	}
	render () {
		const { children, cssMods, caption, canFullScreen } = this.props;
		const { isFullScreen, hover } = this.state;

		return (
			<div className={cx('sp-media-container', cssMods)}>
                <div className={cx('sp-media', {'show-pointer': canFullScreen})} 
				 	 onClick={this.toggleFullScreen}
				 	 onMouseOver={this.handleMouseOver}
				 	 onMouseOut={this.handleMouseOut}>
                	{children}

                	{caption && caption.length > 0 &&
                		<span className={cx('media-caption', {'active': hover})}>{caption}</span>
                	} 
                </div>

                { isFullScreen && 
	                <div className={cx('sp-media-fullscreen-container', {'show-pointer': canFullScreen})}
	                	 onClick={this.toggleFullScreen} >
		                <div className={cx('sp-media')}>
		                	{children}
                	
		                	{caption && caption.length > 0 &&
		                		<span className='media-caption'>{caption}</span>
		                	}
		                </div>
	                </div>
	            }
            </div>
		);
	}

	toggleFullScreen(setFullScreen) {
		if(this.props.canFullScreen) {
			if(typeof setFullScreen === 'boolean'){
				this.setState({isFullScreen: setFullScreen});
			}else{
				this.setState({isFullScreen: !this.state.isFullScreen});
			}
		}
	}

	handleMouseOver() {
		this.setState({hover: true});
	}

	handleMouseOut() {
		this.setState({hover: false});
	}
}