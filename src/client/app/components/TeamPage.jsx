import React from 'react';
import {render} from 'react-dom';

export default class TeamPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    static get defaultProps() {
        return {
        }
    }

    render () {
        return (
            <div className='sp-creators'>
                <span className='sp-creator-container'>
                    <div className='sp-creator-portrait'><img src='./media/portraits/peter.jpg' /></div>
                    <span className='sp-creator-name'>Peter Aquila</span>
                </span>

                <span className='sp-creator-container'>
                    <div className='sp-creator-portrait'><img src='./media/portraits/arend.jpg' /></div>
                    <span className='sp-creator-name'>Arend Castelein</span>
                </span>

                <span className='sp-creator-container'>
                    <div className='sp-creator-portrait'><img src='./media/portraits/nicholas.jpg' /></div>
                    <span className='sp-creator-name'>Nicholas Shooter</span>
                </span>

                <span className='sp-creator-container'>
                    <div className='sp-creator-portrait'><img src='./media/portraits/larry.jpg' /></div>
                    <span className='sp-creator-name'>Larry Smith</span>
                </span>

                <span className='sp-creator-container'>
                    <div className='sp-creator-portrait'><img src='./media/portraits/daniel.jpg' /></div>
                    <span className='sp-creator-name'>Daniel Xiao</span>
                </span>
            </div>
        );
    }
}