//other imports
require('./styles/index.less');

//react imports
import React from 'react';
import {render} from 'react-dom';
// import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

/*pages*/

/* components */
import Header from './components/Header.jsx';
import Footer from './components/Footer.jsx';
import ImageGallery from './components/ImageGallery.jsx';
import Drawer from './components/Drawer.jsx';
import TeamPage from './components/TeamPage.jsx';

class Index extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            // currPage: Header.PAGE.HOME,
        };

        this.onFacebookLinkDidClick = this.onFacebookLinkDidClick.bind(this);
    }
    
    render () {
        let { currPage } = this.state;

        return (
        	<div className='sp-content'>
                {/*<Header 
                    promiseHeaderClick={this.handleHeaderClickActions.bind(this)}
                    currPage={currPage} />*/}

                <Header />

                <div className='sp-body'>
                    <Drawer title='Gallery'>
                        <ImageGallery layout={ImageGallery.LAYOUT.ONEBIG} />
                    </Drawer>

                    <Drawer title='Information'
                            wrapperCls='social-media-links'>
                        <span className='social-item'>
                            <p className='big-text-description'>SOON</p>
                            <img className='external-logo' src='./media/steam-xxl.png' />
                        </span>

                        <span className='social-item show-pointer' onClick={this.onFacebookLinkDidClick}>
                            <p className='big-text-description'>FB</p>
                            <img className='external-logo' src='./media/facebook.png' />
                        </span>
                    </Drawer>

                    <Drawer title='Team'>
                        <TeamPage />
                    </Drawer>
                </div>

                <div className='body-divider'></div>

                <Footer />
    		</div>
    	);
    }     

    handleHeaderClickActions(action) {
        switch(action) {
            case Header.PAGE.HOME:
                this.switchToPage(action);
                break;
            case Header.PAGE.ABOUT:
                this.switchToPage(action);
                break;
            case Header.PAGE.MEDIA:
                this.switchToPage(action);
                break;
        }
    }

    onFacebookLinkDidClick() {
        window.open('https://www.facebook.com/Super-Perspective-966829473354233/')
    }

    switchToPage(page) {
        this.setState({currPage: page});
    }
}

render(<Index/>, document.getElementById('app'));