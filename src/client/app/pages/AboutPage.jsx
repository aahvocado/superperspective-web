require('../styles/about.less');

import React from 'react';
import {render} from 'react-dom';

//components
import List from '../components/List.jsx';
import DevListItem from '../components/DevListItem.jsx';

class AboutPage extends React.Component {
	constructor(props) {
		super(props);
		this.state = {

		};
	}
	static get defaultProps() {
		return {
			children: [],
		}
	}
	render () {
		let { children } = this.props;

		return (
			<div className='about-page'>
				<div className='about-inner'>

					<div className='about-body-text'>
						<p>SuperPerspective is a game about exploring the wonderful world in 2D and 3D and solving brain bending dimensional puzzles. Come check out our demo at GDC 2016!</p>					
					</div>

					<h2 className='title-label'>
						our team
					</h2>

					<List
						wrapperCls='sp-about-list'
						data={this.initDevList()}
						listItemComponent={DevListItem}>

					</List>

				</div>
			</div>
		);
	}

	initDevList() {
		return [
			{title:'Peter Aquila', iconPath:'', description:''},
			{title:'Arend Peter Castelein', iconPath:'', description:''},
			{title:'Nicholas Shooter', iconPath:'', description:''},
			{title:'Larry Smith', iconPath:'', description:''},
			{title:'Daniel Xiao', iconPath:'', description:''},
		]
	}
}
export default AboutPage;